from django.forms import ModelForm
from receipts.models import Account, ExpenseCategory, Receipt


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = "__all__"


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = "__all__"


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = "__all__"
